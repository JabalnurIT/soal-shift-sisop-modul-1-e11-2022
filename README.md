# **Laporan Penjelasan Soal Shift Modul 1**
## Sistem Operasi E 2022
### **Kelompok E-11**
**Asisten : Dewangga Dharmawan**  
**Anggota :**
- Jabalnur 5025201241
- Vania Rizky Juliana Wachid 5025201215
- Maula Izza Azizi 5025201104  

## **Daftar Isi Laporan**
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)
- [Kendala](#kendala)

# Soal 1
Variabel ```users``` menyimpan seluruh akun (username dan password) yang telah terdaftar
```bash
users=(`cat ./users/user.txt`)
statusUsername=false
```
Selama input username gagal dikarenakan sudah terdaftar, maka perintah untuk menginput username akan terus dikeluarkan. Input dari user akan disimpan ke variabel ```username``` 
```bash
while  [[ $statusUsername == false ]]
do
	message=""
	echo 'Masukkan Username Anda : '
	read username
...
done
```
Script dibawah ini memeriksa apakah ```username``` yang diinput pengguna sudah terdaftar.   
Setiap baris pada variabel ```users``` yang berisi ```user.txt``` diiterasi dan diambil **nilai usernamenya saja**. Nilai tersebut akan dibandingkan dengan variabel ```username``` yang merupakan input dari pengguna. Jika ternyata ```username``` sudah terdaftar pada ```./users/user.txt``` maka
variabel ```message``` akan bernilai *"REGISTER: ERROR User already exists"*  dan dicatat ke ```log.txt```
```bash
for user in ${users[@]}
do
    if [[ $user == "Username:"* ]]
    then 
        user=${user#*"Username:"}
        echo $user
        if [[ "$user" == "$username" ]]
        then
            message="REGISTER: ERROR User already exists"
            echo $message
            time="$(date +'%m/%d/%y %T') "
            log="${time} ${message}"
            echo $log >> var/log/log.txt
            break
        fi
    fi
```
Jika ```message``` masih kosong, maka ```username``` yang diinput user belum terdaftar, sehingga variabel ```statusUsername``` nilainya true
```bash
if [[ "$message" == "" ]]
	then 
		statusUsername=true
fi
```
Setelah berhasil input username langkah selanjutnya adalah membuat password.   
Selama password yang diinput belum sesuai syarat dan ketentuan, maka pengguna akan terus diminta untuk menginput password
```bash
while [ $passKondisi == false ]
do 
	echo 'Masukkan Password Anda : '
	read -s password
	if [ ${#password} -ge 8 ]
	then
		if  [[ $(echo "$password" | awk '/[a-z]/ && /[A-Z]/') ]]
		then
			if [[ $password =~ ['!@#$%^&*()_+'] ]]; then
				echo "Password Hanya Boleh Terdiri dari Angka dan Huruf (Alphanumeric)"
			else
				if [ $password == $username ]; then
				echo "Password Tidak Boleh Sama Dengan Username"	
				else
					echo "mantap"
					passKondisi = true
				fi
			fi 
		else
			echo "Password Minimal Terdapat 1 Huruf Kapital dan 1 Huruf Kecil" 
		fi
	else
		echo "Password Minimal 8 Karakter"
	fi
done
```
Setelah input ```username``` dan ```password``` berhasil, maka username dan password akan didata ke ```./users/user.txt```
```bash
echo 'Username:'"${username}" >> ./users/user.txt
echo 'Password:'"${password}" >> ./users/user.txt
```
Keluarkan juga statement bahwa register berhasil dan catat di ```./var/log/log.txt```
```bash
message="REGISTER: INFO User "$username" registered successfully"
echo $message
time="$(date +'%m/%d/%y %T') "
log="${time} ${message}"
echo $log >> ./var/log/log.txt
```
Sistem login berada pada ```main.sh```. Jika username yang diinput oleh pengguna tidak terdaftar pada ```user.txt``` maka program akan meminta terus hingga pengguna menginputkan username yang telah terdaftar di ```user.txt```. Setelah itu dilakukan pengecekan password dan hasilnya dicatat ke ```log.txt```
```bash
while [[ $statusUsername == false ]]
do
	echo "Masukkan Username: "
	read username
	x=0
	for user in ${users[@]}
	do
		let x+=1
		if [[ $user == "Username:"* ]]
		then 
			user=${user#*"Username:"}
			if [[ "$user" == "$username" ]]
			then
				statusUsername=true		
			fi
		else
			if [[ $statusUsername == true ]]
			then
				echo "Masukkan Password: "
				read -s password
				user=${user#*"Password:"}
				if [[ "$user" -eq "$password" ]]
				then
					statusLogin=true
					message="LOGIN: INFO User ${username} logged in"
					echo $message
					time="$(date +'%m/%d/%y %T') "
					log="${time} ${message}"
					echo $log >> ./var/log/log.txt
					break	
				else 
					message="LOGIN: ERROR Failed login attempt on user ${username}"
					echo $message
					time="$(date +'%m/%d/%y %T') "
					log="${time} ${message}"
					echo $log >> ./var/log/log.txt
					break	
				fi
			else
				if [[ x -eq jumlah-1 ]]
				then
					echo "Username is not registered"
				fi
			fi
		fi
	done
done
```
Setelah berhasil login, pengguna dapat mengetikkan dua command ```dl N``` dan ```att```  

untuk **function** command ```dl N``` mendownload **N** gambar dari ```https://loremflickr.com/320/240``` dan dimasukkan kedalam sebuah file lalu di zip. Apabila file zip sudah ada, maka isinya akan ditambahkan. Langkah terakhir diberi ```password``` sesuai user yang sedang login
```bash
dl()
{
	date=$(date +'%y-%m-%d')
	folder="${date}_${username}"
	
	if [[ -f "${folder}.zip" ]]
	then
		unzip -o -P "${password}" "${folder}.zip"
		rm "${folder}.zip"
	fi
	
	num=1
	numFile=$(ls "./${folder}/"| wc -l)
	let numFile+=1
	mkdir ${folder}
	while [[ $num -le ${1} ]]
	do
		wget -O "./${folder}/PIC_${numFile}.jpg" http://loremflickr.com/320/240 
		let num+=1
		let numFile+=1
	done
	zip -r -e -P "${password}" "${folder}.zip" "${folder}/"
	rm -r ${folder}
}
```
untuk **function** command ```att``` menghitung jumlah percobaan login yang berhasil maupun tidak. Hal ini dapat dilakukan dengan melihat history ```log.txt``` karena semua catatan login tersimpan disana.
```bash
att()
{
	loginSuccess=0
	loginFailed=0
	loginTries="./var/log/log.txt"
	while IFS= read -r loginTry
	do
		if [[ $loginTry == *"LOGIN: INFO"*${username}* ]]
		then 
			let loginSuccess+=1
		elif [[ $loginTry == *"LOGIN: ERROR"*${username} ]]
		then 
			let loginFailed+=1
		fi
	done < "$loginTries"
	

	echo "Percobaan Berhasil: ${loginSuccess}"
	echo "Percobaan Gagal   : ${loginFailed}"
}
```
# Soal 2
## **Rata-rata Request Perjam**
Pisahkan field dan mulai dari baris kedua. Menggunakan ```substr(2, 1, 14)``` untuk menampilkan 14 karakter dari field kedua. 14 karakter tersebut merupakan tanggal dan jam request dilakukan. Contoh keluaran ```substr(2, 1, 14)``` adalah ```22/Jan/2022:08```
```bash
cat log_website_daffainfo.log | awk -F '\":\"' 'NR>1 {print substr($2,1,14)}'
```
Setelah itu dihitung banyak request perjamnya dengan
```uniq -c```. Contoh keluaran pada 22/Jan/2022:01 adalah ```546 22/Jan/2022:01```
```bash
uniq -c
```
Banyak request ```$1``` di total dan diabgi dengan banyaknya baris atau ```jam``` dan didapat rata - ratanya
```bash
awk '{total+=$1;n++}
```
Digabung saja dengan pipe dan catat di ```ratarata.txt```
```bash
cat log_website_daffainfo.log | awk -F '\":\"' 'NR>1 {print substr($2,1,14)}' | uniq -c | awk '{total+=$1;n++} END {print "Rata-rata serangan adalah sebanyak",total/n,"requests per jam"}' > ./forensic_log_website_daffainfo_log/ratarata.txt
```
## **IP Terbanyak Request dan Jumlah Request**
Pisahkan field dan mulai dari baris kedua. Menggunakan ```substr(1, 2, length($1))``` untuk menampilkan IP address yang merequest
```bash
awk -F '\":\"' 'NR>1 {print substr($1,2,length($1))}'
```
Hitung banyak request per IP address dan sort dari yang terbanyak
```bash
uniq -c | sort -rn
```
Ambil baris pertama saja karena hanya ingin tau IP address yang melakukan paling banyak request
```bash
awk 'NR==1'
```
Gabung semua menggunakan pipe dan cata pada ```result.txt```
```bash
cat log_website_daffainfo.log | awk -F '\":\"' 'NR>1 {print substr($1,2,length($1))}'| uniq -c | sort -rn | awk 'NR==1' | awk '{print "IP yang paling banyak mengakses sever adalah:",$2,"sebanyak",$1,"request\n"}' > ./forensic_log_website_daffainfo_log/result.txt
```
## **Banyak Request yang Menggunakan Curl**
Untuk request yang menggunakan user-agent curl, pada log ada tulisan curlnya. Jadi cukup menghitung baris yang terdapat ```'curl'```. Hasilnya dicatat di ```result.txt```
```bash
cat log_website_daffainfo.log | awk -F '\":\"' 'NR>1 && /curl/ {n++} END {print "Ada",n,"requests yang menggunakan curl sebagai user-agent\n"}' >> ./forensic_log_website_daffainfo_log/result.txt
```
## Daftar IP yang Request Pukul 2 Pagi
```bash
cat log_website_daffainfo.log | awk -F '\":\"' 'NR>1 $$/curl/ {printf (substr($1,2,length($1))" Jam "substr($2,13,2)); if (substr($2,13,2) < 12 ) printf (" pagi\n"); else if (substr($2,13,2) >= 12 && substr($2,13,2) < 18) printf (" siang\n"); else printf (" malam\n")}' >> ./forensic_log_website_daffainfo_log/result.txt
```

# Soal 3
## minute_log.sh
Membuat folder
```bash
mkdir -p "$HOME/log"
```
Menyimpan ```metric1```
```bash
metric1=$(free -m)
```
Menyimpan ```metric2```
```bash
metric2="path path_size $(du -sh $HOME/)"
```
Gabung ```metric```
```bash
metric="$metric1 $metric2"
```
Menyimpan ```time```
```bash
time="$(date +'%Y%m%d%H%M%S')"
```
Untuk menghapus karakter menggunakan ```sed 's/://g'```, menghapus tulisan /cache/ menggunakan ```sed 's/\/cache//g'```, mengubah ke lowercase menggunakan ```tr '[:upper:]' '[:lower:]'```

Format penulisan dirubah sesuai soal
```bash
i = 1; 
while (i < 3)
{ 
	j = 1;
	while (j < 7)
	{
		if (i == 2 && j > 3)
		{
			break;
		}
		printf ($(7*i)"_"$j",");
		j++
	}
	i++;
}
printf ($18","$19"\n");
i = 8; 
while (i < 18)
{ 
	if (i != 14)
	{ 
		printf ($i",");
	}
	i++;
}
printf ($21","$20"\n");
```
Karena sebelumnya semua huruf dibuat lowercase, maka huruf G pada kolom path_size dibesarkan dengan membesarkan huruf terakhir tiap baris menggunakan ```sed 's/.$/\U&/'```  
Karena huruf terakhir pada baris pertama yaitu pada tulisan path_sizE, e nya menjadi besar, maka seluruh E dikecilkan menggunakan ```sed 's/E/e/g'```  
Lalu simpan hasilnya ke ```metrics_$time.log```  
Gabung semua menggunakan pipe

##aggregate_minutes_to_hourly_log.sh
Menyimpan ```time```
```bash
time="$(date +'%Y%m%d%H')"
```
Jam dikurangi satu, karena data yang diproses ada lah data satu jam sebelumnya
```bash
let time-=1
```
Simpan seluruh nama file yang ada dalam folder ```log``` dengan nama yang sesuai lalu keluarkan isi file dari list file sebelumnya dan isinya disimpan ke variable 
```newMetrics```
```bash
newMetrics=$(ls $HOME/log/metrics_$time* | awk '{system("cat " $1)}')
```
Ambil isi yang hanya memiliki nilai dan simpan kedalam ```valueMetrics``` dengan hanya mengambil lines genap saja. Lalu ubah seluruh ```,``` menjadi ```spasi```. Setelah itu ambil hanya karakter ```1-(n-3), dan (n-1) + 1```
```bash
valueMetrics=$(echo "$newMetrics" | sed -n 'n;p' | sed 's/,/ /g' | awk '{print substr($0,1,length($0)-3)","substr($0,length($0)-1,1)}')
```
Simpan header dari data yang ada
```bash
headerMetrics=$(echo "$newMetrics" | sed -n 'p;n' | sed 's/,/ /g' | awk -F '\t' 'NR==1 && NF=1' )
```
Tambahkan tulisan ```"type "``` ke dalam header Metrics
```bash
headerMetrics=$(echo "$newMetrics" | sed -n 'p;n' | sed 's/,/ /g' | awk 'NR==1 {print}' )
```
Ambil nilai min, max, dan avg dari masing-masing kolom lalu ubah tulisan ```.``` menjadi ```,```. Seluruh hasilnya di simpan kedalam file sementara
```bash
echo "$valueMetrics" | awk '{print $1}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_total.txt
echo "$valueMetrics" | awk '{print $2}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_used.txt
echo "$valueMetrics" | awk '{print $3}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_free.txt
echo "$valueMetrics" | awk '{print $4}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_shared.txt
echo "$valueMetrics" | awk '{print $5}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_buff.txt
echo "$valueMetrics" | awk '{print $6}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_available.txt
echo "$valueMetrics" | awk '{print $7}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_total.txt
echo "$valueMetrics" | awk '{print $8}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_used.txt
echo "$valueMetrics" | awk '{print $9}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_free.txt
echo "$valueMetrics" | awk '{print $10}' | awk 'BEGIN {max = 1} {if (max<4) print $1"-";max++}' > path.txt
echo "$valueMetrics" | awk '{print $11}' | sed 's/[,gG]//g' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's_\([0-9]\)\([0-9]\)_\1,\2G_' > path_size.txt
printf "minimum-\nmaximum-\naverage-" > type.txt
```
Gabung seluruh isi dari file sementara tadi kedalam variable ```bodyMetrics```
```bash
bodyMetrics=$(paste type.txt mem_total.txt mem_used.txt mem_free.txt mem_shared.txt mem_buff.txt mem_available.txt swap_total.txt swap_used.txt swap_free.txt path.txt path_size.txt)
```
Ambil isi dari ```bodyMetrics``` lalu ganti karakter ```"- "``` menjadi ```","```. Ubah karakter ```,``` pada sebelum kata *maximum* dan *average* menjadi *newline*. Lalu hapus karakter ```"-"``` diakhir paragraf
```bash
bodyMetrics=$(echo $bodyMetrics | sed 's/- /,/g' | sed 's/,maximum/\nmaximum/g' | sed 's/,average/\naverage/g' | sed 's/G-/G/g')
```

Hapus seluruh file sementara yang sudah dibuat
```bash
rm -f type.txt mem_total.txt mem_used.txt mem_free.txt mem_shared.txt mem_buff.txt mem_available.txt swap_total.txt swap_used.txt swap_free.txt path.txt path_size.txt 
```

Tampilkan isi headerMetrics yang digabung dengan ```bodyMetrics``` lalu ganti ```spasi``` yang tersisa menjadi ```","```. Terakhir kirim isi variabel ke file ```metrics_agg_$time.log```
```bash
echo -e "$headerMetrics\n$bodyMetrics" | sed 's/[\ ]/,/g' > $HOME/log/metrics_agg_$time.log
```
### Bukti ss Cron
![Bukti ss Cron](https://cdn.discordapp.com/attachments/752127406344634449/949949283606605844/Screen_Shot_2022-03-06_at_3.35.53_PM.png)

# Kendala
- Harus membuka man bash berkali kali karena tidak disediakan di modul
- Bingung harus ngapain, search di googlenya juga susah
- Beberapa command tidak jalan di MacOS sehingga ditengah-tengah perngerjaan harus ganti linux
- Kurang mengerti log error shell script ketika di jalankan seperti contohnya error karena kurang spasi
- Soal no 2 kurang jelas untuk jam 12.00. Karena dari 00.00 hingga 12.00 sudah ganti jam 13 kali. Namun pada kunci jawaban hanya 12 jam
