#!/bin/bash
# Menyimpan
time="$(date +'%Y%m%d%H')"
# Mengurangi jam sebanyak satu, karena data yang diproses ada lah data satu jam sebelumnya
let time-=1
# Menyimpan seluruh nama file yang ada dalam folder log dengan nama yang sesuai | Mengeluarkan isi file dari list file sebelumnya | isinya disimpan ke variable newMetrics
newMetrics=$(ls $HOME/log/metrics_$time* | awk '{system("cat " $1)}')
# Mengambil hanya isi yang memiliki nilai dan menyimpannya kedalam valueMetrics dengan hanya mengambil lines genap saja | Mengubah seluruh , menjadi spasi | Mengambil hanya karakter 1-(n-3), dan (n-1) + 1
valueMetrics=$(echo "$newMetrics" | sed -n 'n;p' | sed 's/,/ /g' | awk '{print substr($0,1,length($0)-3)","substr($0,length($0)-1,1)}')
# Menyimpan headir dari data yang ada
headerMetrics=$(echo "$newMetrics" | sed -n 'p;n' | sed 's/,/ /g' | awk -F '\t' 'NR==1 && NF=1' )
# Menambahkan tulisan "type " ke dalam header Metrics
headerMetrics=$(echo "$newMetrics" | sed -n 'p;n' | sed 's/,/ /g' | awk 'NR==1 {print}' )

# Mengambil nilai min, max, dan avg dari masing-masing kolom | menyubah tulisan . menjadi , Seluruh hasilnya di simpan kedalam file sementara
echo "$valueMetrics" | awk '{print $1}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_total.txt
echo "$valueMetrics" | awk '{print $2}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_used.txt
echo "$valueMetrics" | awk '{print $3}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_free.txt
echo "$valueMetrics" | awk '{print $4}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_shared.txt
echo "$valueMetrics" | awk '{print $5}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_buff.txt
echo "$valueMetrics" | awk '{print $6}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > mem_available.txt
echo "$valueMetrics" | awk '{print $7}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_total.txt
echo "$valueMetrics" | awk '{print $8}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_used.txt
echo "$valueMetrics" | awk '{print $9}' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's/,/./g' > swap_free.txt
echo "$valueMetrics" | awk '{print $10}' | awk 'BEGIN {max = 1} {if (max<4) print $1"-";max++}' > path.txt
echo "$valueMetrics" | awk '{print $11}' | sed 's/[,gG]//g' | awk 'BEGIN {max = 0;min = 100000000} {if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; n++} END {print min"-\n"max"-\n"sum/n"-"}' | sed 's_\([0-9]\)\([0-9]\)_\1,\2G_' > path_size.txt
printf "minimum-\nmaximum-\naverage-" > type.txt

# Menggabungkan seluruh isi dari file sementara tadi kedalam variable bodyMetrics 
bodyMetrics=$(paste type.txt mem_total.txt mem_used.txt mem_free.txt mem_shared.txt mem_buff.txt mem_available.txt swap_total.txt swap_used.txt swap_free.txt path.txt path_size.txt)

# Mengambil isi dari bodyMetrics | mengganti tulisan "- " menjadi "," | mengubah tulisan , pada sebelum kata maximum dan average menjadi newline | menghapus tulisan "-" diakhir paragraph
bodyMetrics=$(echo $bodyMetrics | sed 's/- /,/g' | sed 's/,maximum/\nmaximum/g' | sed 's/,average/\naverage/g' | sed 's/G-/G/g')

# Menghapus seluruh file sementara yang sudah dibuat
rm -f type.txt mem_total.txt mem_used.txt mem_free.txt mem_shared.txt mem_buff.txt mem_available.txt swap_total.txt swap_used.txt swap_free.txt path.txt path_size.txt 

# Menampilkan isi headerMetrics yang digabung dengan bodyMetrics | mengganti spasi yang tersisa menjadi "," | Mengirim isi variabel ke file metrics_agg_$time.log
echo -e "$headerMetrics\n$bodyMetrics" | sed 's/[\ ]/,/g' > $HOME/log/metrics_agg_$time.log

# Memgubah akses hanya boleh dilihat oleh user
chmod 400 $HOME/log/metrics_agg_$time.log

# Setel Cron di dalam Crontab untuk menjalankan program setiap jam
# 0 * * * * bash $HOME/Sisop/Shift/soal-shift-sisop-modul-1-E11-2022/soal3/aggregate_minutes_to_hourly_log.sh

