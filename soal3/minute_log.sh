#!/bin/bash

# Membuat Folder
mkdir -p "$HOME/log"
# Menyimpan metric1
metric1=$(free -m)
# Menyimpan metric2
metric2="path path_size $(du -sh $HOME/)"
# Menggabungkan metric
metric="$metric1 $metric2"
# Menyimpan current time untuk jadi nama file
time="$(date +'%Y%m%d%H%M%S')"
# Menampilkan isi metric | menghapus karakter : | menghapus tulisan /cache/ | membuat semua tulisan menjadi lower Case | Mengubah format penulisan metric sesuai soal
echo $metric | sed 's/://g' | sed 's/\/cache//g' | tr '[:upper:]' '[:lower:]' | awk '{ 
                                                                                        i = 1; 
                                                                                        while (i < 3)
                                                                                        { 
                                                                                            j = 1;
                                                                                            while (j < 7)
                                                                                            {
                                                                                                if (i == 2 && j > 3)
                                                                                                {
                                                                                                    break;
                                                                                                }
                                                                                                printf ($(7*i)"_"$j",");
                                                                                                j++
                                                                                            }
                                                                                            i++;
                                                                                        }
                                                                                        printf ($18","$19"\n");
                                                                                        i = 8; 
                                                                                        while (i < 18)
                                                                                        { 
                                                                                            if (i != 14)
                                                                                            { 
                                                                                                printf ($i",");
                                                                                            }
                                                                                            i++;
                                                                                        }
                                                                                        printf ($21","$20"\n");
                                                                                        
                                                                                    }' | sed 's/.$/\U&/' | sed 's/E/e/g' > $HOME/log/metrics_$time.log
                                                                                    # Karena sebelumnya semua huruf dibuat lowercase, maka huruf G pada kolom path_size dibesarkan dengan membesarkan huruf terakhir tiap baris
                                                                                    # Karena huruf terakhir pada baris pertama yaitu pada tulisan path_sizE, e nya menjadi besar, maka seluruh E dikecilkan
                                                                                    # Menyimpan Hasilnya ke metrics_$time.log

# Memgubah akses hanya boleh dilihat oleh user
chmod 400 $HOME/log/metrics_$time.log

# Setel Cron di dalam Crontab untuk menjalankan program setiap jam
# * * * * * bash $HOME/Sisop/Shift/soal-shift-sisop-modul-1-E11-2022/soal3/minute_log.sh