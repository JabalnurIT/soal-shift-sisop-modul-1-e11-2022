#!/bin/bash

users=(`cat ./users/user.txt`)
statusUsername=false


while  [[ $statusUsername == false ]]
do
	message=""
	echo 'Masukkan Username Anda : '
	read username

	for user in ${users[@]}
	do
		if [[ $user == "Username:"* ]]
		then 
			user=${user#*"Username:"}
			# echo $user
			if [[ "$user" == "$username" ]]
			then
				message="REGISTER: ERROR User already exists"
				echo $message
				time="$(date +'%m/%d/%y %T') "
				log="${time} ${message}"
				echo $log >> ./var/log/log.txt
				break
			fi
		fi
	done
	if [[ "$message" == "" ]]
	then 
		statusUsername=true
	fi
done


passKondisi=false

while [ $passKondisi == false ]
do 
	echo 'Masukkan Password Anda : '
	read -s password
	if [ ${#password} -ge 8 ]
	then
		if  [[ $(echo "$password" | awk '/[a-z]/ && /[A-Z]/') ]]
		then
			if [[ $password =~ ['!@#$%^&*()_+'] ]]; then
				echo "Password Hanya Boleh Terdiri dari Angka dan Huruf (Alphanumeric)"
			else
				if [ $password == $username ]; then
				echo "Password Tidak Boleh Sama Dengan Username"	
				else
					passKondisi = true
				fi
			fi 
		else
			echo "Password Minimal Terdapat 1 Huruf Kapital dan 1 Huruf Kecil" 
		fi
	else
		echo "Password Minimal 8 Karakter"
	fi
done
	
# Menyimpan User ke user.txt
echo 'Username:'"${username}" >> ./users/user.txt
echo 'Password:'"${password}" >> ./users/user.txt

# Register Berhasil
message="REGISTER: INFO User "$username" registered successfully"
echo $message
time="$(date +'%m/%d/%y %T') "
log="${time} ${message}"
echo $log >> ./var/log/log.txt




