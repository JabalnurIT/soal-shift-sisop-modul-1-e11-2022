#!/bin/bash

## Jumlah User terdaftar
jumlah=`cat ./users/user.txt | wc -l`

## AKSES KE user.txt
users=(`cat ./users/user.txt`)

statusLogin=false
statusUsername=false
while [[ $statusUsername == false ]]
do
	echo "Masukkan Username: "
	read username
	x=0
	for user in ${users[@]}
	do
		let x+=1
		if [[ $user == "Username:"* ]]
		then 
			user=${user#*"Username:"}
			if [[ "$user" == "$username" ]]
			then
				statusUsername=true		
			fi
		else
			if [[ $statusUsername == true ]]
			then
				echo "Masukkan Password: "
				read -s password
				user=${user#*"Password:"}
				if [[ "$user" -eq "$password" ]]
				then
					statusLogin=true
					message="LOGIN: INFO User ${username} logged in"
					echo $message
					time="$(date +'%m/%d/%y %T') "
					log="${time} ${message}"
					echo $log >> ./var/log/log.txt
					break	
				else 
					message="LOGIN: ERROR Failed login attempt on user ${username}"
					echo $message
					time="$(date +'%m/%d/%y %T') "
					log="${time} ${message}"
					echo $log >> ./var/log/log.txt
					break	
				fi
			else
				if [[ x -eq jumlah-1 ]]
				then
					echo "Username is not registered"
				fi
			fi
		fi
	done
done

#define function
dl()
{
	date=$(date +'%y-%m-%d')
	folder="${date}_${username}"
	
	if [[ -f "${folder}.zip" ]]
	then
		unzip -o -P "${password}" "${folder}.zip"
		rm "${folder}.zip"
	fi
	
	num=1
	numFile=$(ls "./${folder}/"| wc -l)
	let numFile+=1
	mkdir ${folder}
	while [[ $num -le ${1} ]]
	do
		wget -O "./${folder}/PIC_${numFile}.jpg" http://loremflickr.com/320/240 
		let num+=1
		let numFile+=1
	done
	zip -r -e -P "${password}" "${folder}.zip" "${folder}/"
	rm -r ${folder}
}

att()
{
	loginSuccess=0
	loginFailed=0
	loginTries="./var/log/log.txt"
	while IFS= read -r loginTry
	do
		if [[ $loginTry == *"LOGIN: INFO"*${username}* ]]
		then 
			let loginSuccess+=1
		elif [[ $loginTry == *"LOGIN: ERROR"*${username} ]]
		then 
			let loginFailed+=1
		fi
	done < "$loginTries"
	

	echo "Percobaan Berhasil: ${loginSuccess}"
	echo "Percobaan Gagal   : ${loginFailed}"
}

command=""
until [[ $command == "exit" ]]
do
	read command
	if [[ $command == "dl"* ]]
	then
		command=${command#*"dl "}
		dl $command
	elif [[ $command == "att" ]]
	then
		att
	fi
done






