#!/bin/bash

mkdir -p forensic_log_website_daffainfo_log

#rata-rata
cat log_website_daffainfo.log | awk -F '\":\"' 'NR>1 {print substr($2,1,14)}' | uniq -c | awk '{total+=$1;n++} END {print "Rata-rata serangan adalah sebanyak",total/n,"requests per jam"}' > ./forensic_log_website_daffainfo_log/ratarata.txt
#ip terbanyak
cat log_website_daffainfo.log | awk -F '\":\"' 'NR>1 {print substr($1,2,length($1))}'| uniq -c | sort -rn | awk 'NR==1' | awk '{print "IP yang paling banyak mengakses sever adalah:",$2,"sebanyak",$1,"request\n"}' > ./forensic_log_website_daffainfo_log/result.txt
#user-agent curl
cat log_website_daffainfo.log | awk -F '\":\"' 'NR>1 && /curl/ {n++} END {print "Ada",n,"requests yang menggunakan curl sebagai user-agent\n"}' >> ./forensic_log_website_daffainfo_log/result.txt
#ip jam 2 pagi
cat log_website_daffainfo.log | awk -F '\":\"' 'NR>1 && /22\/Jan\/2022:02/ {printf ("_"substr($1,2,length($1))" Jam 2 pagi\n")}' | uniq -c | awk -F '\ _' '{print $2}' >> ./forensic_log_website_daffainfo_log/result.txt
